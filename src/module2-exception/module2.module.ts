import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { Module2Service } from './module2.service';
import { Module2Controller } from './module2.controller';
import { Module2Middleware } from '../common/middlewares/module2.middleware';

@Module({
  providers: [Module2Service],
  controllers: [Module2Controller],
})
export class Module2Module implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(Module2Middleware).forRoutes(Module2Controller);
  }
}
