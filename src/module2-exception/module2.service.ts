import { Injectable } from '@nestjs/common';

@Injectable()
export class Module2Service {
  getMessage(): string {
    return 'welcome to the module2 service';
  }
}
