import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  UseFilters,
} from '@nestjs/common';
import { Module2Service } from './module2.service';
import { CustomException } from '../common/exceptions/handlers/custom.exception';
import { CustomExceptionFilter } from '../common/exceptions/filters/custom-exception.filter';

@Controller('module2')
export class Module2Controller {
  constructor(private module2Service: Module2Service) {}

  @Get()
  getMessage(): string {
    return this.module2Service.getMessage();
  }

  @Get('error1')
  error1(): string {
    throw new Error('some error 1');
    return 'error1';
  }

  @Get('error2')
  error2(): string {
    throw new HttpException('some error 2', HttpStatus.FORBIDDEN);
    return 'error2';
  }

  @Get('error3')
  error3(): string {
    throw new HttpException(
      {
        error: {
          code: HttpStatus.CREATED,
          message: 'some error 3',
        },
      },
      HttpStatus.FORBIDDEN,
    );
    return 'error3';
  }

  @Get('error4')
  @UseFilters(CustomExceptionFilter)
  error4() {
    throw new CustomException();
  }

  @Get('error5')
  @UseFilters(CustomExceptionFilter)
  error5() {
    throw new CustomException();
  }
}
