import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app/app.module';
import { globalLogger } from './common/middlewares/globalLogger.moddleware';
import { CustomExceptionFilter } from './common/exceptions/filters/custom-exception.filter';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.use(globalLogger);
  // adding this global filter will catch the error in middleware when
  // visiting /module2/error4, and form the response data from the filter.
  // without adding this global filter, the exception in the middleware won't
  // be able to use this filter, because this filter will be registered only at
  // the route handler (check /module2/error4, its registered there),
  // and exception will be thrown at the middleware, (and middleware is called before
  // accessing the route handler), so the exception will be caught by the
  // BaseExceptionFilter instead of the CustomExceptionFilter.
  // app.useGlobalFilters(new CustomExceptionFilter());
  await app.listen(3000);
}

bootstrap();
