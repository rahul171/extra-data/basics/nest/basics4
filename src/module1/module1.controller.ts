import { Controller, Get } from '@nestjs/common';
import { Module1Service } from './module1.service';

@Controller('module1')
export class Module1Controller {
  constructor(private module1Service: Module1Service) {}

  @Get()
  getMessage(): string {
    return this.module1Service.getMessage();
  }

  @Get('route1')
  route1(): string {
    return 'route1';
  }

  @Get('route2')
  route2(): string {
    return 'route2';
  }

  @Get('route3')
  route3(): string {
    return 'route3';
  }

  @Get('route4')
  route4(): string {
    return 'route4';
  }
}
