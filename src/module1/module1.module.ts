import { Module } from '@nestjs/common';
import { Module1Service } from './module1.service';
import { Module1Controller } from './module1.controller';

@Module({
  providers: [Module1Service],
  controllers: [Module1Controller],
})
export class Module1Module {}
