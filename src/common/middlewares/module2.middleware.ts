import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { CustomException } from '../exceptions/handlers/custom.exception';

@Injectable()
export class Module2Middleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    console.log('----Module2Middleware----');

    const url = req.url;

    console.log('Module2Middleware => use => url =>', url);

    if (url.includes('module2/error5')) {
      // if CustomExceptionFilter is registered globally, then this exception
      // will be caught by it, otherwise it will be caught by
      // the BaseExceptionFilter.
      // but if the error is thrown inside a route handler (/module2/error4)
      // rather than the middleware, then it will get caught by
      // CustomExceptionFilter, because CustomExceptionFilter is
      // registered for that route handler.
      throw new CustomException();
    }

    console.log('----END Module2Middleware----');
    next();
  }
}
