import { Request, Response, NextFunction } from 'express';

export function globalLogger(req: Request, res: Response, next: NextFunction) {
  console.log('globalLogger', `${req.method} ${req.url}`);
  next();
}
