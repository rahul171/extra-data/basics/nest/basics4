import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { CustomException } from '../handlers/custom.exception';

@Catch(CustomException)
export class CustomExceptionFilter implements ExceptionFilter {
  catch(exception: CustomException, host: ArgumentsHost) {
    const message = exception.getResponse();
    const status = exception.getStatus();

    const response = host.switchToHttp().getResponse();

    response.status(status).json({ CustomExceptionFilter: { message } });
  }
}
