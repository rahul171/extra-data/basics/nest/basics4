import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { AppService } from './app.service';
import { AppController } from './app.constructor';
import { Module1Module } from '../module1/module1.module';
import { RequestLoggerMiddleware } from '../common/middlewares/requestLogger.middleware';
import { LoggerService } from '../common/services/logger.service';
import { Module1Controller } from '../module1/module1.controller';
import { requestLogger } from '../common/middlewares/requestLoggerFunction.middleware';
import { Module2Module } from '../module2-exception/module2.module';

@Module({
  imports: [Module1Module, Module2Module],
  providers: [AppService, LoggerService],
  controllers: [AppController],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(RequestLoggerMiddleware, requestLogger)
      .exclude(
        { path: 'module1/route2', method: RequestMethod.GET },
        { path: 'module1/route3', method: RequestMethod.GET },
      )
      .forRoutes(Module1Controller);
  }
}
